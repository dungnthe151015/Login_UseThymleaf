package com.example.login_usethymleaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginUseThymleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoginUseThymleafApplication.class, args);
    }

}
